/**
 * File Name: hbfeedbackDAL.js
 *
 * Revision History:
 *       Hadi Baghdadi, 2021-03-06 : Created
 */

var Review = {
    hbInsert: function(options, callback){
        function txFunction(tx){
            var sql = "INSERT INTO review(businessName, typeId, reviewerEmail, reviewerComments, reviewDate, hasRating, rating1, rating2, rating3) VALUES(?, ?, ?, ?, ?, ?, ?,?,?); ";
            tx.executeSql(sql, options,callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: Insert transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    hbUpdate: function (options, callback){
        function txFunction(tx){
            var sql = "UPDATE review SET businessName=?,typeId=?,  reviewerEmail=?, reviewerComments=?,reviewDate=?, hasRating=?, rating1=?, rating2=?, rating3=? WHERE id=?);";
            tx.executeSql(sql, options,callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: Update transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    hbdelete: function(options, callback){
        function txFunction(tx) {
            var sql = "DELETE FROM review WHERE id=?;";
            tx.executeSql(sql, options, callback, errorHandler );
        }
        function successTransaction() {
            console.info("Success: Delete transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    hbSelect: function(options, callback){
        function txFunction(tx) {
            var sql = "SELECT * FROM review WHERE id=?;";
            tx.executeSql(sql, options, callback, errorHandler );
        }
        function successTransaction() {
            console.info("Success: Select transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    hbSelectAll: function(options, callback){
        function txFunction(tx) {
            var sql = "SELECT * FROM review;";
            tx.executeSql(sql, options, callback, errorHandler );
        }
        function successTransaction() {
            console.info("Success: Insert transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    }
};
var Type ={
    SelectAll: function(options, callback){
        function txFunction(tx) {
            var sql = "SELECT * FROM type;";
            tx.executeSql(sql, options, callback, errorHandler );
        }
        function successTransaction() {
            console.info("Success: Insert transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    hbInsert: function (options, callback) {
        function txFunction(tx) {
            var sql = "INSERT INTO type(name) VALUES(?);";
            tx.executeSql(sql, options, callback, errorHandler);
        }

        function successTransaction() {
            console.info("Success: Insert transaction successful");
        }

        db.transaction(txFunction, errorHandler, successTransaction);
    }
}
