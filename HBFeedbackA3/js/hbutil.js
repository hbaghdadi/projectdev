/**
 * File Name: hbutil.js
 *
 * Revision History:
 *       Hadi Baghdadi, 2021-03-06 : Created
 */

function doValidate_frmAddReview() {
    var frmAdd = $("#frmAddReview");

    frmAdd.validate({
        rules:{
            hbTxtAddBusinessName:{
                required: true,
                minlength: 2,
                maxlength: 30
            },
            hbTxtAddReviewerEmail: {
                required: true,
                email: true
            },
            hbTxtReviewDate: {
                required: true
            },
            hbTxtAddFoodQuality: {
                min: 0,
                max: 5
            },

            hbTxtAddService: {
                min: 0,
                max: 5
            },
            hbTxtAddValue: {
                min: 0,
                max: 5
            }
        },
        messages: {
            hbTxtAddBusinessName: {
                required: "Business Name is required",
                minlength: "Length must be 2-30 characters long",
                maxlength: "Length must be 2-30 characters long"
            },
            hbTxtAddReviewerEmail: {
                required: "Email address is required",
                email: "Please enter a valid email address"
            },
            hbTxtReviewDate: {
                required: "Review date is required"
            },
            hbTxtAddFoodQuality: {
                min: "Value must be 0-5",
                max: "Value must be 0-5"
            },
            hbTxtAddService: {
                min: "Value must be 0-5",
                max: "Value must be 0-5"
            },
            hbTxtAddValue: {
                min: "Value must be 0-5",
                max: "Value must be 0-5"
            }
        }
    });
    return frmAdd.valid();
}
function doValidate_frmEditReview() {
    var frmEdit = $("#frmEditReview");

    frmEdit.validate({
        rules:{
            hbTxtModifyBusinessName:{
                required: true,
                minlength: 2,
                maxlength: 30
            },
            hbTxtModifyReviewerEmail: {
                required: true,
                email: true
            },
            hbTxtReviewDateModify: {
                required: true
            },
            hbTxtModifyFoodQuality: {
                min: 0,
                max: 5
            },

            hbTxtModifyService: {
                min: 0,
                max: 5
            },
            hbTxtModifyValueValue: {
                min: 0,
                max: 5
            }
        },
        messages: {
            hbTxtModifyBusinessName: {
                required: "Business Name is required",
                minlength: "Length must be 2-30 characters long",
                maxlength: "Length must be 2-30 characters long"
            },
            hbTxtModifyReviewerEmail: {
                required: "Email address is required",
                email: "Please enter a valid email address"
            },
            hbTxtReviewDateModify: {
                required: "Review date is required"
            },
            hbTxtModifyFoodQuality: {
                min: "Value must be 0-5",
                max: "Value must be 0-5"
            },
            hbTxtModifyService: {
                min: "Value must be 0-5",
                max: "Value must be 0-5"
            },
            hbTxtModifyValueValue: {
                min: "Value must be 0-5",
                max: "Value must be 0-5"
            }
        }
    });
    return frmEdit.valid();
}


