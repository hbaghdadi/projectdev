/**
 * File Name: hbfacade.js
 *
 * Revision History:
 *       Hadi Baghdadi, 2021-03-06 : Created
 */

function hbinitTypeDB() {
    type1 = ["Asian"];
    type2 = ["Canadian"];
    type3 = ["Others"];
    function callback() {
        console.info("Database types added successfully")
    }
    Type.hbInsert(type1, callback);
    Type.hbInsert(type2, callback);
    Type.hbInsert(type3, callback);

}
function hbAddFeedback() {
    if(doValidate_frmAddReview()){
        var businessName = $("#hbTxtAddBusinessName").val();
        var type = $("#cmbType").index();
        var reviewerEmail = $("#hbTxtAddReviewerEmail").val();
        var reviewerComments = $("#hbTxtAddReviewerComments").val();
        var reviewDate = $("#hbTxtReviewDate").val();
        var hasRating;
        var foodQuality = 0;
        var service = 0;
        var value = 0;

        if($("#hbCbAddRating").prop("checked") ==true){
            hasRating = "true";
            foodQuality = parseInt($("#hbTxtAddFoodQuality").val());
            service = parseInt($("#hbTxtAddService").val());
            value = parseInt($("#hbTxtAddValue").val());
        }
        else {
            hasRating = "false";
        }

        function callback(){
            alert("new feedback added");
        }
        options = [businessName, type, reviewerEmail, reviewerComments, reviewDate, hasRating, foodQuality, service, value];
        Review.hbInsert(options, callback);
    }
}

function hbGetReview() {
    $("#hbLvAll").html("Empty");
    $("#hbLvAll").listview("refresh");


    var options = [];


    function callback(tx, results) {
        var htmlCode = "";
        for (var i = 0; i < results.rows.length; i++) {
            var row = results.rows[i];
            var overallRating = parseInt((row['rating1'] + row['rating2'] + row['rating3']) * (100/15));

            htmlCode += "<li><a data-role='button' data-row-id=" + row['id'] + " href='#'>" +
                "<h1>Business Name: " + row['businessName'] + "</h1>" +
                "<h2>Reviewer Email: " + row['reviewerEmail'] + "</h2>" +
                "<h3>Comments: " + row['reviewerComments'] + "</h3>" +
                "<h3>Overall Rating: " + overallRating + "</h3>" +
                "</a></li>";
        }

        var lv = $("#hbLvAll");
        lv = lv.html(htmlCode);
        lv.listview("refresh"); // very important

        function clickHandler() {
            localStorage.setItem("id", $(this).attr("data-row-id"));
            $(location).prop('href', '#hbEditFeedbackPage');
            hbshowCurrentReview();
        }

        $("#hbLvAll a").on("click", clickHandler);


    }

    Review.hbSelectAll(options, callback);
}

function hbshowCurrentReview() {

    var id = localStorage.getItem("id");
    var options = [id];

    function callback(tx, results){
        var row = results.rows[0];
        $("#hbTxtModifyBusinessName").val(row['businessName']);
        $("#hbTxtModifyReviewerEmail").val(row['reviewerEmail']);
        $("#hbTxtModifyReviewerComments").val(row['reviewerComments']);
        $("#hbTxtReviewDateModify").val(row['reviewDate']);
        if(row['hasRating'] == 'true'){
            $("#hbCbModifyRating").prop("checked",true);
            $("#hbhiddenTextboxesModify").show();
            $("#hbTxtModifyFoodQuality").val(row['rating1']);
            $("#hbTxtModifyService").val(row['rating2']);
            $("#hbTxtModifyValueValue").val(row['rating3']);
        }
        else {
            $("#hbCbModifyRating").prop("checked",false);
            $("#hbhiddenTextboxesModify").hide();
        }

    }
    Review.hbSelect(options, callback)
}

function hbUpdateFeedback(){
    if(doValidate_frmEditReview()) {


        var id = localStorage.getItem("id");
        var hasRating;
        var businessName = $("#hbTxtModifyBusinessName").val();
        var reviewerEmail = $("#hbTxtModifyReviewerEmail").val();
        var typeId = $("#cmbTypeModify").index();
        var reviewerComments = $("#hbTxtModifyReviewerComments").val();
        var reviewDate = $("#hbTxtReviewDateModify").val();
        var foodQuality = 0;
        var service = 0;
        var value = 0;
        if ($("#hbCbModifyRating").prop("checked") == true) {
            hasRating = "true";
            foodQuality = parseInt($("#hbTxtModifyFoodQuality").val());
            service = parseInt($("#hbTxtModifyService").val());
            value = parseInt($("#hbTxtModifyValueValue").val());
        } else {
            hasRating = "false"
        }

        function callback() {
            alert("record updated successfully")
        }

        var options = [businessName, typeId, reviewerEmail, reviewerComments, reviewDate, hasRating, foodQuality, service, value, id];

        Review.hbUpdate(options, callback);
    }
}

function hbdeleteFeedback(){
    var id = localStorage.getItem("id");
    var options = [id];
    function callback(){
        alert("record deleted successfully");
        $(location).prop('href', '#hbViewFeedbackPage');
    }
    Review.hbdelete(options, callback);
}

function hbClearDatabase(){
    var result = confirm("Are you sure you want to clear the database");
    if (result){
        try{
            DB.hbDropTables();
            alert("database cleared")
        }
        catch(e){
            alert(e);
        }
    }
}
