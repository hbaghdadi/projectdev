/**
 * File Name: hbglobal.js
 *
 * Revision History:
 *       Hadi Baghdadi, 2021-03-06 : Created
 */



function hbBtnSave_click() {
    doValidate_frmAddReview();
    hbAddFeedback();
}
function hbBtnUpdate_click(){
    doValidate_frmEditReview();
    hbUpdateFeedback();
}


function hbBtnSaveDefault_click() {
    var email = $('#hbTxtAddDefaultReviewerEmail').val();
    localStorage.setItem("DefaultEmail", email);
    alert("Default reviewer email saved");

}

function hbClearDatabase_click() {
    hbClearDatabase();
}


function hbBtnDelete_click() {
    hbdeleteFeedback();
}

function init() {
    $(function () {
        $("#hbCbAddRating").click(function () {
            if ($(this).is(":checked")) {
                $("#hbhiddenTextboxes").show();
            } else {
                $("#hbhiddenTextboxes").hide();
            }
        });
    });
    $(function () {
        $("#hbCbModifyRating").click(function () {
            if ($(this).is(":checked")) {
                $("#hbhiddenTextboxesModify").show();
            } else {
                $("#hbhiddenTextboxesModify").hide();
            }
        });
    });

    $("#hbTxtAddFoodQuality").change(function() {
        var foodQuality = parseInt($("#hbTxtAddFoodQuality").val());
        var value = parseInt($("#hbTxtAddValue").val());
        var service = parseInt($("#hbTxtAddService").val());
        $("#hbTxtAddOverallRating").val((foodQuality + service + value) * (100/15) + "%");
    });
    $("#hbTxtAddService").change(function() {
        var foodQuality = parseInt($("#hbTxtAddFoodQuality").val());
        var value = parseInt($("#hbTxtAddValue").val());
        var service = parseInt($("#hbTxtAddService").val());
        $("#hbTxtAddOverallRating").val((foodQuality + service + value) * (100/15) + "%");
    });
    $("#hbTxtAddValue").change(function() {
        var foodQuality = parseInt($("#hbTxtAddFoodQuality").val());
        var value = parseInt($("#hbTxtAddValue").val());
        var service = parseInt($("#hbTxtAddService").val());
        $("#hbTxtAddOverallRating").val((foodQuality + service + value) * (100/15) + "%");
    });


    $("#hbTxtModifyFoodQuality").change(function() {
        var foodQualityModify = parseInt($("#hbTxtModifyFoodQuality").val());
        var valueModify = parseInt($("#hbTxtModifyValueValue").val());
        var serviceModify = parseInt($("#hbTxtModifyService").val());
        $("#hbTxtModifyOverallRating").val((foodQualityModify + serviceModify + valueModify) * (100/15) + "%");
    });
    $("#hbTxtModifyService").change(function() {
        var foodQualityModify = parseInt($("#hbTxtModifyFoodQuality").val());
        var valueModify = parseInt($("#hbTxtModifyValueValue").val());
        var serviceModify = parseInt($("#hbTxtModifyService").val());
        $("#hbTxtModifyOverallRating").val((foodQualityModify + serviceModify + valueModify) * (100/15) + "%");
    });
    $("#hbTxtModifyValueValue").change(function() {
        var foodQualityModify = parseInt($("#hbTxtModifyFoodQuality").val());
        var valueModify = parseInt($("#hbTxtModifyValueValue").val());
        var serviceModify = parseInt($("#hbTxtModifyService").val());
        $("#hbTxtModifyOverallRating").val((foodQualityModify + serviceModify + valueModify) * (100/15) + "%");
    });

    $("#hbClearDatabase").on("click", hbClearDatabase_click);
    $("#hbBtnSave").on("click", hbBtnSave_click);
    $("#hbBtnUpdate").on("click", hbBtnUpdate_click);
    $("#hbButtonSave").on("click", hbBtnSaveDefault_click);
    $("#hbAddFeedBackPage").on("pageshow", function (){
        $('#hbTxtAddReviewerEmail').val(localStorage.getItem("DefaultEmail"));
    });
    $("#hbViewFeedbackPage").on("pageshow", function (){
        hbGetReview();
    });
    $("#hbBtnDelete").on("click", hbBtnDelete_click);


}

function initDB() {
    try {
        DB.hbCreateDatabase();
        if (db) {
            DB.hbDropTables();
            DB.hbCreateTables();
            hbinitTypeDB();
        }
        else {
            console.error("Error: Cannot create tables because database does not exist");
        }
    }
    catch (e) {
        console.error("Error: Error occurred in initDB function. Cannot proceed");
    }
}

$(document).ready(function () {
    init();
    initDB();

});
