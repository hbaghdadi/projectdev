/**
 * File Name: hbdatabase.js
 *
 * Revision History:
 *       Hadi Baghdadi, 2021-03-06 : Created
 */
var db;

function errorHandler(error) {
    console.error("SQL error:    " + error.message);
}

var DB = {
    hbCreateDatabase: function (){
        var shortName = "hbFeedback";
        var version = "3.0";
        var displayName = "DB For Feedback App";
        var dbSize = 2 * 1024 * 1024;

        function hbCreateSuccess(){
            console.info("Success: Database created successfully");
        }
        db = openDatabase(shortName, version, displayName, dbSize, hbCreateSuccess);
    },
    hbCreateTables: function (){
        function txFunction(tx){
            var sqlType = "CREATE TABLE IF NOT EXISTS type(" +
                "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                "name VARCHAR (20) NOT NULL);";
            var sqlReview =
                "CREATE TABLE IF NOT EXISTS review(" +
                "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                "businessName VARCHAR(30) NOT NULL," +
                "typeId INTEGER NOT NULL," +
                "reviewerEmail VARCHAR(30)," +
                "reviewerComments TEXT," +
                "reviewDate DATE," +
                "hasRating VARCHAR(1)," +
                "rating1 INTEGER," +
                "rating2 INTEGER," +
                "rating3 INTEGER," +
                "FOREIGN KEY(typeId) REFERENCES type(id));";
            var optionsType = [];
            var optionsReview = [];

            function successCallback(){
                console.info("Success: Creating tables successful");
            }
            tx.executeSql(sqlType, optionsType, successCallback, errorHandler);
            tx.executeSql(sqlReview, optionsReview, successCallback, errorHandler);
        }
        function successTransaction(){
            console.info("Success: Tables created successfully")
        }
        db.transaction(txFunction, errorHandler, successTransaction);

    },
    hbDropTables:  function () {
        function txFunction(tx) {
            var sqlType = "DROP TABLE IF EXISTS type;";
            var sqlReview = "DROP TABLE IF EXISTS review;";
            var optionsType = [];
            var optionsReview = [];


            function successCallback() {
                console.info("Success: Dropping tables successful");
            }

            tx.executeSql(sqlType, optionsType, successCallback, errorHandler);
            tx.executeSql(sqlReview, optionsReview, successCallback, errorHandler);
        }

        function successTransaction() {
            console.info("Success: Dropping table transaction is successful");
        }

        db.transaction(txFunction, errorHandler, successTransaction);
    }

}
